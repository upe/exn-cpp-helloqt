#***************************************************************************
#*   Autoversion makefile                   v.20230620.220121 (qmake-qt5)  *
#*   Copyright (C) 2014-2023 by Ruben Carlo Benante <rcb@beco.cc>          *
#*                                                                         *
#*   This makefile sets BUILD and allows to set MAJOR.MINOR version,       *
#*   DEBUG and OBJ to compile a range of different targets                 *
#***************************************************************************
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; version 2 of the License.               *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************
#*   To contact the author, please write to:                               *
#*   Ruben Carlo Benante                                                   *
#*   Email: rcb@beco.cc                                                    *
#*   Webpage: http://beco.cc/                                              *
#***************************************************************************
#
# Usage:
#    make clean : remove arquivos automaticos
#    make ex7.out :
#         - compila e cria ex7.out, e para isso
#         - antes cria Makefile, e para isso
#         - antes cria helloqt.pro, sem dependencias, fazendo:
#             + varredura na pasta
#             + adicionando linha QT += requisitos...
#             + trocando target

# Regra para limpar a pasta de trabalho (working dir)
clean:
	rm Makefile ex7.o ex7.out helloqt.pro ui_ex7.h

# Regra para criar ex7.out, dependendo de Makefile
ex7.out: Makefile
	make -f Makefile

# Regra para criar Makefile, dependendo de helloqt.pro
Makefile: helloqt.pro
	qmake-qt5 helloqt.pro

# Regra para criar helloqt.pro, nao depende de nada
helloqt.pro:
	qmake-qt5 -project
	echo "QT += gui widgets" >> helloqt.pro
	sed -i 's/TARGET = .*/TARGET = ex7.out/' helloqt.pro

#* ------------------------------------------------------------------- *
#* makefile config for Vim modeline                                    *
#* vi: set ai noet ts=4 sw=4 tw=0 wm=0 fo=croqlt list :                *
#* Template by Dr. Beco <rcb at beco dot cc> Version 20220521.001405   *

